//
// Created by Григорий Жаворонков on 12.10.2020.
//

#ifndef LAB4_TESTS_H
#define LAB4_TESTS_H

#include "linked_list.h"
#include <stdio.h>

void foreachTest(linked_list* list);

void mapTest(linked_list* list);

void mapMutTest(linked_list* list);

void foldlTest(linked_list* list);

void iterateTest();

void saveLoadTest(linked_list* list, const char* filename);

void serializeDeserializeTest(linked_list* list, const char* filename);

#endif //LAB4_TESTS_H
