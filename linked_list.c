#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include "linked_list.h"

linked_list* list_create(int number) {
    linked_list* new_list = (linked_list*) malloc(sizeof(linked_list));
    new_list->value = number;
    new_list->next = 0;
    return new_list;
}

linked_list* list_add_front(linked_list** list, int number) {
    linked_list* new_head = list_create(number);
    new_head->next = *list;
    *list = new_head;
    return new_head;
}

linked_list* list_add_back(linked_list** list, int number) {
    linked_list* current = *list;
    while (current->next != 0) {
        current = current->next;
    }
    linked_list* new_element = list_create(number);
    current->next = new_element;
    return new_element;
}

int list_get(linked_list* list, size_t id) {
    linked_list* current = list;
    size_t counter;
    for (counter = 0; counter < id; counter++) {
        if (current->next != 0) {
            current = current->next;
        } else {
            break;
        }
    }
    if (counter == id) {
        return current->value;
    } else {
        return 0;
    }
}

void list_free(linked_list* list) {
    linked_list* current = list;
    while (current->next != 0) {
        linked_list* next = current->next;
        free(current);
        current = next;
    }
    free(current);
}

size_t list_length(linked_list* list) {
    linked_list* current = list;
    size_t counter = 1;
    while (current->next != 0) {
        counter++;
    }
    return counter;
}

linked_list* list_node_at(linked_list* list, size_t id) {
    linked_list* current = list;
    size_t counter;
    for (counter = 0; counter < id; counter++) {
        if (current->next != 0) {
            current = current->next;
        } else {
            break;
        }
    }
    if (counter == id) {
        return current;
    } else {
        return NULL;
    }
}

linked_list* list_copy(linked_list* list) {
    linked_list* current = list;
    linked_list* copied = list_create(current->value);
    while (current->next != 0) {
        current = current->next;
        list_add_back(&copied, current->value);
    }
    return copied;
}

bool list_equals(linked_list* first, linked_list* second) {
    linked_list* first_current = first;
    linked_list* second_current = second;
    if (first_current->value != second_current->value) return false;
    while (first_current->next != 0 && second_current->next != 0) {
        first_current = first_current->next;
        second_current = second_current->next;
        if (first_current->value != second_current->value) return false;
    }
    if (first_current->value != second_current->value) return false;
    if (first_current->next != second_current->next) return false;
    return true;
}

long long list_sum(linked_list* list) {
    linked_list* current = list;
    long long sum = current->value;
    while (current->next != 0) {
        current = current->next;
        sum += current->value;
    }
    return sum;
}

void foreach(linked_list* list, void (f) (int*)) {
    linked_list* current = list;
    f(&current->value);
    while (current->next != 0) {
        current = current->next;
        f(&current->value);
    }
}

linked_list* map(linked_list* list, int (f) (int)) {
    linked_list* current = list;
    linked_list* result = list_create(f(current->value));
    while (current->next != 0) {
        current = current->next;
        list_add_back(&result, f(current->value));
    }
    return result;
}

void map_mut(linked_list* list, int (f) (int)) {
    linked_list* current = list;
    current->value = f(current->value);
    while (current->next != 0) {
        current = current->next;
        current->value = f(current->value);
    }
}

void foldl(linked_list *list, int* accumulator, int f (int, int)) {
    linked_list* current = list;
    *accumulator = f(current->value, *accumulator);
    while (current->next != 0) {
        current = current->next;
        *accumulator = f(current->value, *accumulator);
    }
}

linked_list* iterate(int s, size_t length, int f (int)) {
    linked_list* result = list_create(s);
    int value = s;
    for (size_t i = 1; i < length; i++) {
        value = f(value);
        list_add_back(&result, value);
    }
    return result;
}

bool save(linked_list* list, const char* filename) {
    errno = 0;
    FILE* file = fopen(filename, "w");
    if (errno || ferror(file)) return false;
    linked_list* current = list;
    fprintf(file, "%d", current->value);
    if (errno || ferror(file)) return false;
    while (current->next != 0) {
        current = current->next;
        fprintf(file, " %d", current->value);
        if (errno || ferror(file)) return false;
    }
    fflush(file);
    fclose(file);
    if (errno || ferror(file)) return false;
    return true;
}

bool load(linked_list** list, const char* filename) {
    errno = 0;
    FILE* file = fopen(filename, "r");
    if (errno || ferror(file)) return false;
    int input;
    fscanf(file, "%d", &input);
    if (errno || ferror(file)) return false;
    *list = list_create(input);
    while(fscanf(file, "%d", &input) != EOF) {
        if (errno || ferror(file)) return false;
        list_add_back(list, input);
    }
    fclose(file);
    if (errno || ferror(file)) return false;
    return true;
}

bool serialize(linked_list* list, const char* filename) {
    errno = 0;
    linked_list* current = list;
    FILE* file = fopen(filename, "wb");
    if (errno || ferror(file)) return false;
    fwrite(&current->value, sizeof(int), 1, file);
    while (current->next != 0) {
        current = current->next;
        fwrite(&current->value, sizeof(int), 1, file);
        if (errno || ferror(file)) return false;
    }
    fflush(file);
    fclose(file);
    if (errno || ferror(file)) return false;
    return true;
}

bool deserialize(linked_list** list, const char* filename) {
    errno = 0;
    FILE* file = fopen(filename, "rb");
    if (errno || ferror(file)) return false;
    int input;
    list_free(*list);
    fread(&input, sizeof(int), 1, file);
    if (errno || ferror(file)) return false;
    *list = list_create(input);
    while (fread(&input, sizeof(int), 1, file) != 0) {
        if (errno || ferror(file)) return false;
        list_add_back(list, input);
    }
    fclose(file);
    if (errno || ferror(file)) return false;
    return true;
}
