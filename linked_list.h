#ifndef LAB4_LINKED_LIST_H
#define LAB4_LINKED_LIST_H

#include <stdbool.h>
#include <stdlib.h>

typedef struct linked_list linked_list;

struct linked_list {
    int value;
    linked_list* next;
};

linked_list* list_create(int number);

linked_list* list_add_front(linked_list** list, int number);

linked_list* list_add_back(linked_list** list, int number);

int list_get(linked_list* list, size_t id);

void list_free(linked_list* list);

size_t list_length(linked_list* list);

linked_list* list_node_at(linked_list* list, size_t id);

long long list_sum(linked_list* list);

linked_list* list_copy(linked_list* list);

bool list_equals(linked_list* first, linked_list* second);

void foreach(linked_list* list, void (f) (int*));

linked_list* map(linked_list* list, int (f) (int));

void map_mut(linked_list* list, int (f) (int));

void foldl(linked_list *list, int* accumulator, int f (int, int));

linked_list* iterate(int s, size_t length, int f (int));

bool save(linked_list* list, const char* filename);

bool load(linked_list** list, const char* filename);

bool serialize(linked_list* list, const char* filename);

bool deserialize(linked_list** list, const char* filename);



#endif //LAB4_LINKED_LIST_H
