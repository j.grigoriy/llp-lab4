#include <stdio.h>
#include "linked_list.h"
#include "tests.h"

const char* filename = "./file.txt";

int main() {
    linked_list* list = 0;
    size_t length;
    printf("Введите количество чисел: ");
    scanf("%zd", &length);
    printf("Введите числа:\n");
    for (size_t i = 0; i < length; i++) {
        int input;
        scanf("%d", &input);
        list_add_front(&list, input);
    }
    foreachTest(list);
    mapTest(list);
    mapMutTest(list);
    foldlTest(list);
    iterateTest();
    saveLoadTest(list, filename);
    serializeDeserializeTest(list, filename);
    return 0;
}
