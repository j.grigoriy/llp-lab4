all: program

program: main.c linked_list.c tests.c
	gcc -o main main.c linked_list.c tests.c

run: program
	./main

clean:
	rm main
	rm file.txt