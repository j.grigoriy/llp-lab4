#include "tests.h"

void f_print_list_whitespaces(int* value) {
    printf("%d ",  *value);
}

void f_print_list_newline(int* value) {
    printf("%d\n", *value);
}

int f_map_square(int value) {
    return value * value;
}

int f_map_cube(int value) {
    return value * value * value;
}

int f_map_mut_abs(int value) {
    return value >= 0 ? value : (value * -1);
}

int f_foldl_max_element(int value, int accumulator) {
    return value > accumulator ? value : accumulator;
}

int f_foldl_min_element(int value, int accumulator) {
    return value < accumulator ? value : accumulator;
}

int f_foldl_sum(int value, int accumulator) {
    return value + accumulator;
}

int f_iterate_power2 (int value) {
    return value * 2;
}

void foreachTest(linked_list* list) {
    printf("===== FOREACH TEST BEGIN =====\n\n");
    printf("PRINTING WITH WHITESPACES\n");
    foreach(list, f_print_list_whitespaces);

    printf("\n\nPRINTING WITH NEWLINE\n");
    foreach(list, f_print_list_newline);

    printf("\n=====  FOREACH TEST END  =====");
}

void mapTest(linked_list* list) {
    printf("\n\n===== MAP TEST BEGIN =====\n");
    printf("LIST: ");
    foreach(list, f_print_list_whitespaces);
    printf("\nMAPPED SQUARED LIST: ");
    foreach(map(list, f_map_square), f_print_list_whitespaces);
    printf("\nMAPPED CUBED LIST: ");
    foreach(map(list, f_map_cube), f_print_list_whitespaces);
    printf("\n=====  MAP TEST END  =====\n");
}

void mapMutTest(linked_list* list) {
    printf("\n===== MAP MUT TEST BEGIN =====\n");
    linked_list* mutable = list_copy(list);
    printf("LIST: ");
    foreach(list, f_print_list_whitespaces);
    printf("\nABS LIST: ");
    map_mut(mutable, f_map_mut_abs);
    foreach(mutable, f_print_list_whitespaces);
    printf("\n=====  MAP MUT TEST END  =====\n");
}

void foldlTest(linked_list* list) {
    printf("\n===== FOLDL TEST BEGIN =====\n");
    printf("LIST: ");
    foreach(list, f_print_list_whitespaces);
    int acc = list->value;
    foldl(list, &acc, f_foldl_max_element);
    printf("\nMAX VALUE USING FOLDL: %d", acc);
    acc = list->value;
    foldl(list, &acc, f_foldl_min_element);
    printf("\nMIN VALUE USING FOLDL: %d", acc);
    acc = 0;
    foldl(list, &acc, f_foldl_sum);
    printf("\nSUM OF ELEMENTS USING FOLDL: %d", acc);
    printf("\n=====  FOLDL TEST END  =====\n");
}

void iterateTest() {
    printf("\n===== ITERATE TEST BEGIN =====\n");
    printf("RESULT LIST: ");
    foreach(iterate(1, 10, f_iterate_power2), f_print_list_whitespaces);
    printf("\n=====  ITERATE TEST END  =====\n");
}

void saveLoadTest(linked_list* list, const char* filename) {
    printf("\n===== SAVE/LOAD TEST BEGIN =====\n");
    printf("LIST: ");
    foreach(list, f_print_list_whitespaces);
    printf("\nSAVING...");
    save(list, filename);
    printf("\nLOADING...");
    linked_list* loaded;
    load(&loaded, filename);
    printf("\nLOADED LIST: ");
    foreach(loaded, f_print_list_whitespaces);
    printf("\nEQUALS?: %s", list_equals(list, loaded) ? "TRUE" : "FALSE");
    printf("\n=====  SAVE/LOAD TEST END  =====\n");
}

void serializeDeserializeTest(linked_list* list, const char* filename) {
    printf("\n===== SERIALIZE / DESERIALIZE TEST BEGIN =====\n");
    printf("LIST: ");
    foreach(list, f_print_list_whitespaces);
    printf("\nSERIALIZNIG...");
    serialize(list, filename);
    printf("\nDESERIALIZING...");
    linked_list* loaded;
    deserialize(&loaded, filename);
    printf("\nDESERIALIZED LIST: ");
    foreach(loaded, f_print_list_whitespaces);
    printf("\nEQUALS?: %s", list_equals(list, loaded) ? "TRUE" : "FALSE");
    printf("\n=====  SERIALIZE / DESERIALIZE TEST END  =====\n");
}